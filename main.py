import os
import copy
import json

from app.utils.draw import generate_gif
from app.graph_traverse import BFS, DFS


def main():
    config_path = os.environ.get('PATH_TO_CONFIG') or \
                  './data/incoherent_graph_dataset'

    with open(config_path) as f:
        data = json.load(f)

    states_DFS = DFS(copy.deepcopy(data))
    generate_gif(states_DFS, 'dfs')
    states_BFS = BFS(copy.deepcopy(data))
    generate_gif(states_BFS, 'bfs')
    print(states_BFS == states_DFS)


if __name__ == "__main__":
    main()

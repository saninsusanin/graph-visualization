from enum import Enum
from graphviz import Graph


class Color(Enum):
    not_visited = 'gray77'
    watched = 'gray26'
    visited = 'blue4'


def get_current_state(graph):
    tmp = {}

    for vertex_id in graph:
        tmp[vertex_id] = graph[vertex_id]

    return tmp


def get_graph_from_state(state):
    graph = Graph(format='png')
    edges = {}

    for vertex_id in state:
        graph.node(vertex_id,
                   color=state[vertex_id]['color'],
                   fontcolor=state[vertex_id]['color'],
                   shape='circle',
                   style='filled')

        for neighbour in state[vertex_id]['neighbors']:
            edge = neighbour + vertex_id \
                if neighbour < vertex_id \
                else vertex_id + neighbour
            edges[edge] = True

    graph.edges(edges.keys())

    return graph

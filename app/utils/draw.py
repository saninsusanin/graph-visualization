import io

from PIL import Image
from app.utils.utils import get_graph_from_state


def graph_to_bytes(graph):
    graph.graph_attr["rankdir"] = "LR"

    return graph.pipe()


def generate_gif(states, name):
    gif = []

    for state in states:
        graph = get_graph_from_state(state)
        image = Image.open(io.BytesIO(graph_to_bytes(graph)))
        gif.append(image)

    if len(gif) != 0:
        gif[0].save(f'visualisations/{name}.gif',
                    save_all=True,
                    append_images=gif[1:],
                    optimize=False,
                    duration=400,
                    loop=0)

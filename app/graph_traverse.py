import os
import copy

from app.data_structures.queue import Queue
from app.data_structures.stack import Stack
from app.utils.utils import get_current_state, Color


def graph_traverse(graph, data_structure):
    config = os.getenv('CONFIG', 'TESTING')
    states = []

    for vertex_data in graph.values():

        if vertex_data['color'] != Color.visited.value:
            vertex_data['color'] = Color.watched.value
            data_structure.push(vertex_data)

            while data_structure:

                if config == 'TESTING':
                    states.append(copy.deepcopy(get_current_state(graph)))

                popped = data_structure.pop()

                for neighbor in popped['neighbors']:

                    if graph[neighbor]['color'] == Color.not_visited.value:
                        graph[neighbor]['color'] = Color.watched.value
                        data_structure.push(graph[neighbor])

                popped['color'] = Color.visited.value

            if config == 'TESTING':
                states.append(copy.deepcopy(get_current_state(graph)))

    return states


def BFS(graph):
    return graph_traverse(graph, Queue())


def DFS(graph):
    return graph_traverse(graph, Stack())

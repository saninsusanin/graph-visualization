from .abstract_structure import AbstractStructure


class Stack(AbstractStructure):

    def pop(self):
        return self.data.pop()

from collections import deque
from abc import abstractmethod


class AbstractStructure:

    def __init__(self, data=()):
        self.data = deque(data)

    def push(self, element):
        self.data.append(element)

    @abstractmethod
    def pop(self):
        pass

    def __bool__(self):
        return bool(self.data)

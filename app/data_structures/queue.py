from .abstract_structure import AbstractStructure


class Queue(AbstractStructure):

    def pop(self):
        return self.data.popleft()

# Graph Visualization
# About 
This application visualize dfs and bfs algorithms

# Requirements 
+ graphviz
+ Pillow
# How to run
1. Clone repository
2. Go to the cloned directory  
3. Download requirements:  
    `pip3 install -r requirements`
4. If graphviz is not installed properly:
    1. Mac: `brew install graphviz`
    2. Ubuntu: `sudo apt-get install graphviz`
         
# How to use
+ To start application run such command in your terminal:  
python3 main.py  
+ To see result of visualisation set environment variable CONFIG to TESTING:  

import os
import json
import unittest

from app.graph_traverse import BFS, DFS


class Tests(unittest.TestCase):
    def setUp(self):
        config_path = os.environ.get('PATH_TO_CONFIG') or './tests/test_config'

        with open(config_path) as f:
            self.config = json.load(f)

    def abstract_test(self, algorithm):
        method_name = str(algorithm).split()[1]

        with open(self.config[method_name]) as f:
            config = json.load(f)

        for current_test in config:

            with open(current_test) as f:
                data = json.load(f)

            states = algorithm(data)
            self.assertEqual(str(states), config[current_test],
                             f"error in {method_name} - {current_test}")

    def test_bfs(self):

        self.abstract_test(BFS)

    def test_dfs(self):

        self.abstract_test(DFS)


if __name__ == "__main__":
    unittest.main()
